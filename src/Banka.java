import java.io.IOException;
import java.util.Scanner;
import java.util.*;


/**
 * Created by fikrican on 8.12.14.
 */
public class Banka
{
    private String bankaIsmi;
    private Scanner giris;

    private boolean bDevam = true;
    private boolean bHsp = true;

    Banka() throws IOException {};

    Banka(String banka_ismi) throws IOException
    {
        bankaIsmi = banka_ismi;
    }

    int musteriIslemleri() throws IOException
    {

        Hesaplar[] pHesaplars = new Hesaplar[999];
        giris = new Scanner(System.in);

        System.out.println("\t ASDASDASDASDASD");
        System.out.println("==========================================");
        System.out.println("Hesabınız var mi?\n 1)Evet\n 2)Hayir");
        if( giris.nextInt() == 1)
            bDevam = true;
        else
            bDevam = false;

        if(bDevam)
        {
            int hesapIndeks;

            System.out.println("\n Hesap numaranızı giriniz :");
            hesapIndeks = giris.nextInt();
            System.out.println("\n Sifrenizi giriniz :");
            int sifrekontrol = giris.nextInt();

            if(pHesaplars[hesapIndeks + 1].getSifre() == sifrekontrol)
            {
                bHsp = true;
            }
            else
            {
                System.out.println("Yanlis sifre girdiniz!!!");
                return musteriIslemleri();
            }


            /*for (int k = 0; k < pHesaplars.length; k++)
            {
                if (pHesaplars[k] != null)
                    System.out.println(k + " ) " + pHesaplars[k].get_musteri_ismi() + "\nHesap Numarasi : " + pHesaplars[k].getHesap_no());
            }
            hesapIndeks = giris.nextInt();*/

            if(bHsp)
            {
                System.out.println("\nYapmak istediginiz islemi seciniz\n1) Para Yatirma\n2) Para Cekme\n3) Hesap Dokumu");
                int secim = giris.nextInt();

                if ( secim == 1)
                {
                    System.out.println("Yatirmak istediginiz miktari giriniz =  ");
                    pHesaplars[hesapIndeks].paraYatirma(giris.nextDouble());
                }
                else if ( secim == 2)
                {
                    System.out.println("Cekmek istediginiz miktari giriniz =  ");
                    if( pHesaplars[hesapIndeks].paraCekme(giris.nextDouble()) == -1)
                    {
                        System.out.println("Bakiye yetersiz");
                    }
                    else
                        System.out.println("Paranizi alin");
                }
                else if ( secim == 3)
                {
                    System.out.println("Bakiye =  " + String.format("%.2f", pHesaplars[hesapIndeks].getBakiye()) );
                }
                else
                    System.out.println("Yanlis secim yaptiniz");

                System.out.println("Bu hesaptan devam etmek istiyor musunuz?\n1 ) Evet\n2 ) Hayir");
                if( giris.nextInt() == 1)
                    bHsp = true;
                else bHsp = false;
            }
            bHsp = true;

            System.out.println("Baska bir islem yapmak ister misiniz?\n1 ) Evet\n2 ) Hayir");
            if (giris.nextInt() == 1) bDevam = true;
            else
                return 0;

        }

        else {

            String tmp_musteri_ismi;
            double tmp_bakiye;
            long tmp_hesap_no;
            int tmp_sifre;
            int i = 0;

            System.out.println("Kullanicinin ismini giriniz : ");
            tmp_musteri_ismi = giris.nextLine();
            giris.nextLine();

            System.out.println("4 haneli sifrenizi giriniz : ");
            tmp_sifre = giris.nextInt();
            giris.nextLine();

            tmp_hesap_no = i + 1;

            tmp_bakiye = 0;

            pHesaplars[i] = new Hesaplar(new Musteri(tmp_musteri_ismi, tmp_sifre), tmp_bakiye, tmp_hesap_no);
            pHesaplars[i].musteriKaydet(tmp_musteri_ismi, tmp_sifre, tmp_bakiye, tmp_hesap_no);

            System.out.println("Hesap eklendi!\n");
            return musteriIslemleri();
        }




        return 0;
    }

    public static void main(String[] args) throws IOException
    {
        Banka myBank = new Banka("AMK BANKASI");
        System.exit( myBank.musteriIslemleri() );
    }



}
