import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Created by fikrican on 8.12.14.
 */

public class Hesaplar extends Musteri {

    private Musteri musteri;
    private double bakiye;
    private long hesap_no;

    Hesaplar(){};

    Hesaplar(Musteri m_musteri,double m_bakiye,long m_hesap_no) {

        musteri = m_musteri;
        bakiye = m_bakiye;
        hesap_no = m_hesap_no;

    }

    public void paraYatirma(double miktar) {
        bakiye+=miktar;
    }

    public int paraCekme(double miktar) {
        if(miktar>bakiye)
            return -1;
        else
            bakiye-= miktar;
        return 0;
    }

    public double getBakiye() {
        return bakiye;
    }

    public String get_musteri_ismi() {
        return musteri.getMusteri_ismi();
    }

    public long getHesap_no() {
        return hesap_no;
    }

    public Musteri getMusteri() {
        return musteri;
    }

    public void musteriKaydet(String musteri_ismi,int sifre,double m_bakiye,long m_hesap_no) throws IOException {
        File reg = new File("kayıt.txt");
        if (!reg.exists()) {
            reg.createNewFile();
        }

        FileWriter fileWriter = new FileWriter(reg, true);
        BufferedWriter bWriter = new BufferedWriter(fileWriter);
        bWriter.write(musteri_ismi + "\t");
        bWriter.write(sifre + "\t");
        bWriter.write(m_bakiye + "\t");
        bWriter.write(m_hesap_no + "\n");
        bWriter.close();

    }
}
